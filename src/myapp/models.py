from django.db import models


class DemoModel(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    # image = models.ImageField(upload_to="demo_images")

    def __str__(self):
        return self.title
    
class Friend(models.Model):
    title = models.CharField(max_length=255)
    des = models.TextField()

    def __str__(self):
        return self.title
