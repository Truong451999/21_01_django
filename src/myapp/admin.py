from django.contrib import admin
from .models import DemoModel

class DemoModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')
admin.site.register(DemoModel, DemoModelAdmin)
