from django.views.generic import TemplateView
from .tasks import show_hello_world
from .models import DemoModel, Friend
# Create your views here.


class ShowHelloWorld(TemplateView):
    template_name = 'hello_world.html'

    def get(self, *args, **kwargs):
        # show_hello_world.apply()
        # data = {
        #     'title': 'abc',
        #     'body': 'truong'
        # }
        # DemoModel.objects.create(**data)
        # return super().get(*args, **kwargs)
        
        # demo_data = DemoModel.objects.all()
        item = {
            'title': 'Giá trị từ Python',
        }
        context = {'item': item}
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        # context = super().get_context_data(**kwargs)
        # context['demo_content'] = DemoModel.objects.all()
        # return context
        return {}
    
class ShowDetail(TemplateView):
    template_name = 'Detail.html'

    def get(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        item_id = kwargs.get('item_id')
        # show_hello_world.apply()
        # data = {
        #     'title': 'abc',
        #     'body': 'truong'
        # }
        # DemoModel.objects.create(**data)
        # return super().get(*args, **kwargs)
        
        try:
            item = DemoModel.objects.get(pk=item_id)
        except DemoModel.DoesNotExist:
            item = None
        context = {'item': item}
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        # context = super().get_context_data(**kwargs)
        # context['demo_content'] = DemoModel.objects.all()
        # return context
        return {}
    

class WeddingInvitation(TemplateView):
    template_name = 'wedding_invitation.html'

    def get(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        item_id = kwargs.get('item_id')
        try:
            item = DemoModel.objects.get(pk=item_id)
        except DemoModel.DoesNotExist:
            item = None
        context = {'item': item}
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        # context = super().get_context_data(**kwargs)
        # context['demo_content'] = DemoModel.objects.all()
        # return context
        return {}

from django.shortcuts import render
from django.http import HttpResponseNotFound
def custom_page_not_found(request, exception):
    return render(request, '404.html', status=404)
