"""mydjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path

from myapp.views import ShowHelloWorld, ShowDetail, WeddingInvitation

from django.conf.urls import handler404
from myapp.views import custom_page_not_found

handler404 = custom_page_not_found
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', ShowHelloWorld.as_view()),
    url(r'^(?P<item_id>\d+)/$', ShowDetail.as_view()),
    # url(r'^wedding-invitation/^(?P<item_id>\d+)/$', WeddingInvitation.as_view()),
    url(r'^wedding-invitation/(?P<item_id>\d+)/$', WeddingInvitation.as_view()),
    path('404/', custom_page_not_found, name='custom_404'),
    
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
